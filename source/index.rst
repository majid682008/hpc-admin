.. HPC documentation master file, created by
   sphinx-quickstart on Sun Dec 29 10:48:13 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

راهمنمای مدیریت خوشه محاسباتی دانشکده فیزیک
==============================================

این راهنما برای مدیریت خوشه محاسباتی ایجاد شده است و در حال تکمیل است ...




.. toctree::
   :maxdepth: 1
   :caption: فهرست:

   adduser


اندیسک‌ها و جداول
=================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
