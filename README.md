# HPC-admin

## Clone

Frist we should clone the repository:
```bash
  git clone https://gitlab.com/majid682008/hpc-admin.git
  cd hpc-admin
```
In `Iran` you must use a proxy to access `gitlab.com` web site. I recomend `fod`. You could simply use it following this [link](https://github.com/freedomofdevelopers/fod)

## Virtual Environments
To development mode; Just use steps below:
```bash
  python3 -m venv hpc-venv
  source hpc-venv/bin/activate
  pip install -r requirements.txt
  make html
```
